class ListingConversationsController < ApplicationController

  before_filter do |controller|
   controller.ensure_logged_in t("layouts.notifications.you_must_log_in_to_send_a_message")
  end

  before_filter :fetch_listing_from_params, :except => :different_dates
  before_filter :fetch_listing_from_messages_params, :only => :different_dates
  before_filter :ensure_listing_is_open
  before_filter :ensure_listing_author_is_not_current_user, :except => :different_dates
  before_filter :ensure_authorized_to_reply, :except => :different_dates

  skip_filter :dashboard_only

  def new
    use_contact_view = @listing.status_after_reply == "free"
    if @listing.rental_requestable?(@current_user, @current_community)
      rental_request_params = fetch_rental_request_params
      if rental_request_params
        @rental_request = @listing.rental_requests.build(requester_id: @current_user.id)
        @rental_request.attributes = rental_request_params
      end

      if @rental_request.nil?
        redirect_to listing_path(@listing) and return
      end
    end
    @listing_conversation = new_conversation

    if use_contact_view
      render :contact, locals: {contact: false}
    else
      render :new_with_payment
    end
  end

  def contact
    @listing_conversation = new_conversation
    render :contact, locals: {contact: true}
  end

  def create
    status = @listing.status_after_reply
    if @listing.rental_requestable?(@current_user, @current_community)
      rental_request_params = fetch_rental_request_params
      if rental_request_params
        save_conversation(params[:listing_conversation], status, rental_request_params)
      else
        redirect_to listing_path(@listing)
      end
    else
      save_conversation(params[:listing_conversation], status)
    end
  end

  def different_dates
    @listing_conversation = ListingConversation.find(params[:id])
    rental_request = @listing_conversation.rental_request
    rental_request_params = fetch_rental_request_params
    if rental_request_params
      if rental_request.new_from_date.present? && rental_request.new_to_date.present?
        rental_request.from_date = rental_request.new_from_date
        rental_request.to_date = rental_request.new_to_date
      end
      rental_request.new_from_date = rental_request_params[:from_date]
      rental_request.new_to_date = rental_request_params[:to_date]
      if rental_request.save
        @listing_conversation.status = params[:status]
        @listing_conversation.payment_attributes ={:community_id => @current_community.id, :sum => rental_request.subtotal, :currency => @listing_conversation.listing.currency}
        @listing_conversation.payment.save!

        flash[:notice] = t("layouts.notifications.#{@listing_conversation.discussion_type}_#{@listing_conversation.status}")
        redirect_to person_message_path(:person_id => @current_user.id, :id => @listing_conversation.id)
      else
        flash[:error] = rental_request.errors.full_messages
        redirect_to person_message_path(@current_user, @listing_conversation)
      end
    else
      flash[:error] = t("layouts.notifications.something_went_wrong")
      redirect_to person_message_path(@current_user, @listing_conversation)
    end
  end

  def create_contact
    save_conversation(params[:listing_conversation], "free")
  end

  private

  def fetch_rental_request_params
    if params[:rental_request].present?
      begin
        from_date = Date.strptime(params[:rental_request][:from_date], "%m/%d/%Y")
        to_date = Date.strptime(params[:rental_request][:to_date], "%m/%d/%Y")
        if from_date.present? && to_date.present?
          {from_date: from_date, to_date: to_date}
        end
      rescue

      end
    end
  end

  def save_conversation(params, status, rental_request_params = nil)
    @listing_conversation = new_conversation(params, rental_request_params)

    if @listing_conversation.save
      @listing_conversation.status = status
      flash[:notice] = t("layouts.notifications.message_sent")
      Delayed::Job.enqueue(MessageSentJob.new(@listing_conversation.messages.last.id, @current_community.id, @listing_conversation.rental_request.present?? @listing_conversation.rental_request.id : nil))
      redirect_to session[:return_to_content] || root
    else
      flash[:error] = "Sending the message failed. Please try again."
      redirect_to root
    end
  end

  def ensure_listing_author_is_not_current_user
    if @listing.author == @current_user
      flash[:error] = t("layouts.notifications.you_cannot_send_message_to_yourself")
      redirect_to (session[:return_to_content] || root)
    end
  end

  # Ensure that only users with appropriate visibility settings can reply to the listing
  def ensure_authorized_to_reply
    unless @listing.visible_to?(@current_user, @current_community)
      flash[:error] = t("layouts.notifications.you_are_not_authorized_to_view_this_content")
      redirect_to root and return
    end
  end

  def ensure_listing_is_open
    if @listing.closed?
      flash[:error] = t("layouts.notifications.you_cannot_reply_to_a_closed_#{@listing.direction}")
      redirect_to (session[:return_to_content] || root)
    end
  end

  def fetch_listing_from_params
    @listing = Listing.find(params[:listing_id] || params[:id])
  end

  def fetch_listing_from_messages_params
    conversation = Conversation.find(params[:id])
    @listing = conversation.listing
  end

  def new_conversation(conversation_params = {}, rental_request_params = nil)
    conversation = ListingConversation.new(conversation_params.merge(community: @current_community, listing: @listing))
    conversation.build_starter_participation(@current_user)
    conversation.build_participation(@listing.author)
    if rental_request_params.present?
      conversation.build_rental_request_with(@current_user, @listing, rental_request_params[:from_date], rental_request_params[:to_date])
    end
    conversation
  end
end