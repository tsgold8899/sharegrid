class DiscountRule < ActiveRecord::Base
  attr_accessible :days_range_from, :days_range_to, :deduction_value, :discount_id, :apply_type, :deduction_type, :title, :sort_key
  as_enum :apply_type, [:days], :column => "apply_type_cd"
  as_enum :deduction_type, [:percentage], :column => "deduction_type_cd"

  belongs_to :discount

  def parse_title
    eval("\"#{self.title}\"")
  end

  def parse_deduction_formulas
    if deduction_type == :percentage
      "*(100+(#{self.deduction_value}))/100"
    else
      ""
    end
  end

  def rule_applicable?(dates)
    if apply_type == :days
      if (days_range_from.nil? || dates >= days_range_from) && (days_range_to.nil? || dates <= days_range_to)
        return true
      end
    end
    return false
  end

  def calculate_deduction(price)
    if deduction_type == :percentage
      price*self.deduction_value/100
    else
      0
    end
  end
end
