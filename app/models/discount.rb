class Discount < ActiveRecord::Base
  attr_accessible :title, :active, :started_at, :ended_at

  has_many :transaction_type
  has_many :discount_rules
  def is_active?
    if self.started_at.nil? || self.ended_at.nil?
      self.active
    elsif self.started_at <= Date.today && Date.today <= self.ended_at
      self.active
    else
      false
    end
  end
end
