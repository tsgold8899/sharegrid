class RentalRequest < ActiveRecord::Base
  attr_accessible :from_date, :to_date, :conversation_id, :listing_id, :status, :requester_id, :new_from_date, :new_to_date

  belongs_to :conversation
  belongs_to :listing
  belongs_to :requester, :class_name => "Person", :foreign_key => "requester_id"

  validates :from_date, :presence => true, :date => true
  validates :to_date, :presence => true, :date => { :after => :from_date }
  validates :listing_id, :presence => true
  validates :requester_id, :presence => true
  validate :from_date_cannot_be_overlapped_with_accepted_requests
  validate :to_date_cannot_be_overlapped_with_accepted_requests
  validate :new_dates_cannot_be_same_as_original_dates

  def from_date_cannot_be_overlapped_with_accepted_requests
    overlapped = RentalRequest.where("listing_id = :listing_id AND from_date <= :from_date AND to_date >= :from_date", :listing_id => self.listing_id, :from_date => self.from_date)
    if self.id.present?
      overlapped = overlapped.where("id != ?", self.id)
    end
    if overlapped.exists?
      errors.add(:from_date, I18n.t("listings.rental_request.from_date_cannot_be_overlapped"))
    end
  end

  def to_date_cannot_be_overlapped_with_accepted_requests
    overlapped = RentalRequest.where("listing_id = :listing_id AND from_date <= :to_date AND to_date >= :to_date", :listing_id => self.listing_id, :to_date => self.to_date)
    if self.id.present?
      overlapped = overlapped.where("id != ?", self.id)
    end
    if overlapped.exists?
      errors.add(:to_date, I18n.t("listings.rental_request.until_date_cannot_be_overlapped"))
    end
  end

  def new_dates_cannot_be_same_as_original_dates
    if new_from_date.eql?(from_date) && new_to_date.eql?(to_date)
      errors.add(:new_dates, I18n.t("listings.rental_request.new_dates_cannot_be_same_as_original_dates"))
    end
  end

  include AASM
  aasm column: :status do
    state :pending, initial: true
    state :suggesting_different
    state :submitted_another
    state :accepted
    state :rejected
    state :paid
    state :confirmed
    state :canceled
  end

  scope :pendings, -> { where(status: :pending)}
  scope :accepteds_or_paids_or_confirmeds, -> { where(status: [:accepted, :paid, :confirmed])}
  scope :pendings_or_suggesting_differents_or_submitted_anothers, -> { where(status: [:pending, :suggesting_different, :submitted_another])}

  def self.filter(options = {})
    listing_id, status = options[:listing_id], options[:status]
    if listing_id.present? && status.present?
      where(listing_id: listing_id, status: status)
    elsif listing_id.present?
      where(listing_id: listing_id)
    elsif status.present?
      where(status: status)
    else
      scoped
    end
  end

  def dates_editable?(person)
    if person.nil?
      true
    else
      is_owner = listing.author.id == person.id
      if is_owner
        if self.pending?
          true
        elsif self.submitted_another?
          true
        else
          false
        end
      else
        if !self.id.present?
          true
        elsif self.suggesting_different?
          true
        else
          false
        end
      end
    end
  end

  def days
    if negotiating?
      (new_to_date - new_from_date).to_i + 1
    elsif from_date.present? && to_date.present?
      (to_date - from_date).to_i + 1
    end
  end

  def sum
    listing.price * days
  end

  def applied_discount_rule
    listing.applicable_discount_rule(days)
  end

  def discount_value
    if applied_discount_rule
      applied_discount_rule.calculate_deduction(listing.price) * days
    end
  end

  def subtotal
    discount_value.present?? sum + discount_value : sum
  end

  def service_fee(community)
    payment = community.payment_gateway.new_payment
    payment.community = community
    payment.sum = subtotal
    payment.total_commission
  end

  def owner_profit(community)
    payment = community.payment_gateway.new_payment
    payment.community = community
    payment.sum = subtotal
    payment.seller_gets
  end

  def different_dates_status
    (status == "pending" || status == "submitted_another")? "suggesting_different" : "submitted_another"
  end

  def negotiating?
    suggesting_different? || submitted_another?
  end
end