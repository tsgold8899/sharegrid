@javascript
Feature: Admin edits menu link

  Background:
    Given "kassi_testperson1" has admin rights in community "test"

    And there is a menu link
    And the title is "Blog link" and the URL is "http://blog.sharegrid.com/" with locale "en" for that menu link
    And the title is "Verkkopäiväkirja" and the URL is "http://blog.sharegrid.com/" with locale "fi" for that menu link

    And there is a menu link
    And the title is "Homepage link" and the URL is "http://sharegrid.com/" with locale "en" for that menu link
    And the title is "Kotisivu" and the URL is "http://sharegrid.com/" with locale "fi" for that menu link

    And I am logged in as "kassi_testperson1"
    And I am on the menu links admin page

  Scenario: Admin edits menu link
    And I change field "Blog link" to "Sharegrid Blog"
    And I change field "Verkkopäiväkirja" to "Sharegridn blogi"
    And I change field "Homepage link" to "Sharegrid Homepage"
    And I change field "Kotisivu" to "Sharegridn Kotisivu"
    And I press submit
    Then I should see "Tribe details updated"
    When I open the menu
    Then I should see "Sharegrid Blog" on the menu
    Then I should see "Sharegrid Homepage" on the menu

  Scenario: Admin edits menu link order
    When I click up for menu link "Homepage link"
    And I press submit
    Then I should see "Tribe details updated"
    When I open the menu
    Then I should see "Blog link" on the menu
    And I should see "Homepage link" on the menu
    And I should see "Homepage link" before "Blog link"

  Scenario: Admin removes menu link
    When I open the menu
    Then I should see "Blog link" on the menu

    When I remove menu link with title "Blog link"
    And I press submit
    Then I should see "Tribe details updated"

    When I open the menu
    Then I should not see "Blog link" on the menu