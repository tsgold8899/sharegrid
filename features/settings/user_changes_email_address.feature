Feature: User changes email address
  In order to change the email address associated with me in Sharegrid
  As a user
  I want to be able to change my email address

  Background:
    Given there are following users:
      | person      |
      | sharegrid1 |
    And there are following emails:
      | person      | address                 | send_notifications | confirmed_at        |
      | sharegrid1 | sharegrid@example.com  | false              | 2013-11-14 20:02:23 |
      | sharegrid1 | sharegrid2@example.com | true               | 2013-11-14 20:02:23 |
      | sharegrid1 | sharegrid@gmail.com    | false              | 2013-11-14 20:02:23 |
      | sharegrid1 | sharegrid@yahoo.com    | false              | nil                 |
    And there are following communities:
      | community             | allowed_emails |
      | testcommunity         | @example.com   |
      | anothertestcommunity  | @gmail.com     |
    And "sharegrid1" is a member of community "testcommunity"
    And "sharegrid1" is a member of community "anothertestcommunity"
    And I am logged in as "sharegrid1"
    And I am on the account settings page

  @javascript
  Scenario: User adds a new email (and confirms it)
    When I add a new email "sharegrid1-2@example.com"
    Then I should have unconfirmed email "sharegrid1-2@example.com"
    When I confirm email address "sharegrid1-2@example.com"
    Then I should have confirmed email "sharegrid1-2@example.com"

  @javascript
  Scenario: User removes an email
    Given I will confirm all following confirmation dialogs if I am running PhantomJS
    Then I should not be able to remove email "sharegrid2@example.com"
    Then I should not be able to remove email "sharegrid@gmail.com"
    When I remove email "sharegrid@example.com"
    Then I should not have email "sharegrid@example.com"

  @javascript
  Scenario: User changes notification email
    Then I should not be able to remove notifications from "sharegrid2@example.com"
    When I set notifications for email "sharegrid@example.com"
    Then I should be able to remove notifications from "sharegrid2@example.com"

  @javascript
  Scenario: User resends confirmation mail
    Then I should have unconfirmed email "sharegrid@yahoo.com"
    And I should not be able to resend confirmation for "sharegrid@example.com"
    And I should not be able to resend confirmation for "sharegrid2@example.com"
    And I should not be able to resend confirmation for "sharegrid@gmail.com"
    And I should be able to resend confirmation for "sharegrid@yahoo.com"
    When I resend confirmation for "sharegrid@yahoo.com"
    And "sharegrid@yahoo.com" should have 1 emails
    And I confirm email address "sharegrid@yahoo.com"
    Then I should have confirmed email "sharegrid@yahoo.com"
    And "sharegrid@yahoo.com" should have 1 emails