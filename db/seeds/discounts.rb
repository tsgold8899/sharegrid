d = Discount.find_by_title('default')
if d.nil?
  d = Discount.create!(title: 'default', active: 1)
end
d.discount_rules.delete_all()
DiscountRule.create!(discount_id: d.id, apply_type: DiscountRule::days, deduction_type: DiscountRule::percentage,
              days_range_from: 3, days_range_to: 6, deduction_value: -15, sort_key: 1, title: '#{days_range_from} days - #{-deduction_value}%')
DiscountRule.create!(discount_id: d.id, apply_type: DiscountRule::days, deduction_type: DiscountRule::percentage,
              days_range_from: 7, days_range_to: 13, deduction_value: -36, sort_key: 5, title: '#{days_range_from} days - #{-deduction_value}%')
DiscountRule.create!(discount_id: d.id, apply_type: DiscountRule::days, deduction_type: DiscountRule::percentage,
              days_range_from: 14, days_range_to: 20, deduction_value: -47, sort_key: 10, title: '#{days_range_from} days - #{-deduction_value}%')
DiscountRule.create!(discount_id: d.id, apply_type: DiscountRule::days, deduction_type: DiscountRule::percentage,
              days_range_from: 21, days_range_to: 27, deduction_value: -52, sort_key: 15, title: '#{days_range_from} days - #{-deduction_value}%')
DiscountRule.create!(discount_id: d.id, apply_type: DiscountRule::days, deduction_type: DiscountRule::percentage,
              days_range_from: 28, days_range_to: 34, deduction_value: -54, sort_key: 20, title: '#{days_range_from} days - #{-deduction_value}%')
DiscountRule.create!(discount_id: d.id, apply_type: DiscountRule::days, deduction_type: DiscountRule::percentage,
              days_range_from: 35, days_range_to: 41, deduction_value: -61, sort_key: 25, title: '#{days_range_from} days - #{-deduction_value}%')
DiscountRule.create!(discount_id: d.id, apply_type: DiscountRule::days, deduction_type: DiscountRule::percentage,
              days_range_from: 42, days_range_to: 48, deduction_value: -66, sort_key: 30, title: '#{days_range_from} days - #{-deduction_value}%')
DiscountRule.create!(discount_id: d.id, apply_type: DiscountRule::days, deduction_type: DiscountRule::percentage,
              days_range_from: 49, days_range_to: nil, deduction_value: -70, sort_key: 35, title: '#{days_range_from} days - #{-deduction_value}%')

tt = TransactionType.first
tt.discount_id = d.id
tt.save