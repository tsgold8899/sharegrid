class AddTitleToDiscountRule < ActiveRecord::Migration
  def self.up
    add_column :discount_rules, :title, :string
    add_column :discount_rules, :sort_key, :integer
  end

  def self.down
    remove_column :discount_rules, :title
    remove_column :discount_rules, :sort_key
  end
end
