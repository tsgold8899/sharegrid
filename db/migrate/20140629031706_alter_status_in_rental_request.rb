class AlterStatusInRentalRequest < ActiveRecord::Migration
  def self.up
    rename_column :rental_requests, :status_cd, :status
  end

  def self.down
    rename_column :rental_requests, :status, :status_cd
  end
end
