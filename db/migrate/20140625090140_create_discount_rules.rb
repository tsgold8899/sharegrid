class CreateDiscountRules < ActiveRecord::Migration
  def self.up
    create_table :discount_rules do |t|
      t.integer :discount_id
      t.integer :apply_type_cd, :null => false, :default => 0
      t.integer :deduction_type_cd, :null => false, :default => 0
      t.integer :days_range_from
      t.integer :days_range_to
      t.float :deduction_value
      t.timestamps
    end
  end

  def self.down
    drop_table :discount_rules
  end
end
