class AddSuggestedDatesToRentalRequest < ActiveRecord::Migration
  def self.up
    add_column :rental_requests, :suggested_from_date, :date
    add_column :rental_requests, :suggested_to_date, :date
  end

  def self.down
    remove_column :rental_requests, :suggested_from_date
    remove_column :rental_requests, :suggested_to_date
  end
end
