class CreateRentalRequests < ActiveRecord::Migration
  def self.up
    create_table :rental_requests do |t|
      t.integer :conversation_id, :null => false
      t.date :from_date
      t.date :to_date
      t.timestamps
    end
  end

  def self.down
    drop_table :rental_requests
  end
end