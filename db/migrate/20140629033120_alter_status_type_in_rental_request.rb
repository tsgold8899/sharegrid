class AlterStatusTypeInRentalRequest < ActiveRecord::Migration
  def self.up
    change_column :rental_requests, :status, :string, :null => true, :default => nil
  end

  def self.down
    change_column :rental_requests, :status, :integer, :null => false, :default => 0
  end
end
