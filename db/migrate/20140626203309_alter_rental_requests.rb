class AlterRentalRequests < ActiveRecord::Migration
  def self.up
    add_column :rental_requests, :listing_id, :integer, null: false
    add_column :rental_requests, :status_cd, :integer, null: false, default: 0
  end

  def self.down
    remove_column :rental_requests, :listing_id
    remove_column :rental_requests, :status_cd
  end
end
