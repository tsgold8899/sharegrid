class AddDiscountIdToTransactionType < ActiveRecord::Migration
  def self.up
    add_column :transaction_types, :discount_id, :integer
  end

  def self.down
    remove_column :transaction_types, :discount_id
  end
end