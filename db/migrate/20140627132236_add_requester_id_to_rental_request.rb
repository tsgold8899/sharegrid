class AddRequesterIdToRentalRequest < ActiveRecord::Migration
  def self.up
    add_column :rental_requests, :requester_id, :string, null: false
  end

  def self.down
    remove_column :rental_requests, :requester_id
  end
end
