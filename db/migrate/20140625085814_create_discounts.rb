class CreateDiscounts < ActiveRecord::Migration
  def self.up
    create_table :discounts do |t|
      t.string :title
      t.integer :active, :default => 1
      t.date :started_at
      t.date :ended_at
      t.timestamps
    end
  end

  def self.down
    drop_table :discounts
  end
end
