class RenameSuggestedDatesToNewDates < ActiveRecord::Migration
  def self.up
    rename_column :rental_requests, :suggested_from_date, :new_from_date
    rename_column :rental_requests, :suggested_to_date, :new_to_date
  end

  def self.down
    rename_column :rental_requests, :new_from_date, :suggested_from_date
    rename_column :rental_requests, :new_to_date, :suggested_to_date
  end
end